package se.kare.valprognos.exec;

import java.io.InputStream;
import java.io.InputStreamReader;

import se.kare.valprognos.parse.ValPrognos;

public class ProcessFiles {
	
	public static InputStream getIS(String path) {
		return ProcessFiles.class.getClassLoader().getResourceAsStream(path);
	}

	public static void main(String[] args) throws Exception {
		for(int i = 2000 ; i < 2019 ; i++) {
			InputStream is = getIS(""+i+".src");
			InputStreamReader isr = new InputStreamReader(is);
			Source src = ValPrognos.parseValSRC(isr);
			System.out.println("Sant "+i+" "+(is != null));
		}
	}

}
